FROM python:3.6

RUN apt-get update && apt-get install -y build-essential locales-all && rm -rf /var/lib/apt/lists/*

ADD requirements.txt /
RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /doubletapp
ADD . /doubletapp/

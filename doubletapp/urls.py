from django.contrib import admin
from django.urls import path
from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('categories', views.get_categories),
    path('levels', views.get_levels),
    path('themes', views.get_themes),
    path('themes/<int:id>', views.get_theme_by_id),
    path('words/<int:id>', views.get_word_by_id)
]

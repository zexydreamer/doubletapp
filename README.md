# doubletapp

1. Клонировать репо

```shell
git clone https://gitlab.com/zexydreamer/doubletapp.git
```

2. В файле config.py нужно указать API_SECRET приложения и информацию о базе данных.

3. Сделать миграции

```shell
python manage.py makemigrations
pyhton manage.py migrate
```

4. Собрать статику

```shell
python manage.py collectstatic
```

5. Собрать образ docker

```shell
docker build -t project .
```

6. Запустить образ

```shell
docker run -it -p 8000 project gunicorn doubletapp.wsgi:application --bind 0.0.0.0:8000
```


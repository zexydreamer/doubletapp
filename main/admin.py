from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import *


admin.site.register(Level)


@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    readonly_fields = ['headshot_sound']

    def headshot_sound(self, obj):
        return mark_safe('<audio controls> <source src="{url}"> </audio>'.format(url=obj.sound_url))


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    readonly_fields = ["headshot_image"]

    def headshot_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.photo_url,
            width=obj.photo.width,
            height=obj.photo.height,
        )
        )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ["headshot_image"]

    def headshot_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.icon_url,
            width=obj.icon.width,
            height=obj.icon.height,
        )
        )

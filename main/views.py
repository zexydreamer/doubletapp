from django.http import HttpResponseForbidden, JsonResponse
from django.conf import settings
from .models import *


def get_categories(request):
    try:
        if not check_secret(request):
            return HttpResponseForbidden()
    except KeyError:
        return HttpResponseForbidden()
    categories = Category.objects.all()
    data = list()
    for c in categories:
        data.append(c)
        data[len(data) - 1] = dict()
        element = data[len(data) - 1]
        element['id'] = c.id
        element['name'] = c.name
        element['icon'] = c.icon_url
    return JsonResponse(data, safe=False)


def get_levels(request):
    try:
        if not check_secret(request):
            return HttpResponseForbidden()
    except KeyError:
        return HttpResponseForbidden()
    levels = Level.objects.all()
    data = list()
    for lvl in levels:
        data.append(dict())
        element = data[len(data) - 1]
        element['id'] = lvl.id
        element['name'] = lvl.name
        element['code'] = lvl.code
    return JsonResponse(data, safe=False)


def get_themes(request):
    try:
        if not check_secret(request):
            return HttpResponseForbidden()
    except KeyError:
        return HttpResponseForbidden()
    category = int(request.GET['category'])
    level = int(request.GET['level'])
    theme = Theme.objects.get(category_id=category, level__id=level)
    data = dict()
    data['id'] = theme.id
    data['category'] = theme.category_id
    data['level'] = theme.level.id
    data['name'] = theme.name
    data['photo'] = theme.photo_url
    return JsonResponse(data)


def get_theme_by_id(request, id):
    try:
        if not check_secret(request):
            return HttpResponseForbidden()
    except KeyError:
        return HttpResponseForbidden()
    theme = Theme.objects.get(id=id)
    data = dict()
    data['id'] = theme.id
    data['category'] = theme.category_id
    data['level'] = theme.level.id
    data['name'] = theme.name
    data['photo'] = theme.photo_url
    data['words'] = list()
    temp = Word.objects.all()
    words = [word for word in temp if word.theme_id == id]
    for word in words:
        t = dict()
        t['id'] = word.id
        t['name'] = word.name
        data['words'].append(t)
    return JsonResponse(data)


def get_word_by_id(request, id):
    try:
        if not check_secret(request):
            return HttpResponseForbidden()
    except KeyError:
        return HttpResponseForbidden()
    word = Word.objects.get(id=id)
    data = dict()
    data['id'] = word.id
    data['name'] = word.name
    data['translation'] = word.translation
    data['transcription'] = word.transcription
    data['example'] = word.example
    data['sound'] = word.sound_url
    return JsonResponse(data)


def check_secret(request):
    secret = settings.API_SECRET
    source = request.headers['Secret']
    if source != secret:
        return False
    return True

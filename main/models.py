from django.db import models


class Level(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Word(models.Model):
    name = models.CharField(max_length=50)
    translation = models.CharField(max_length=50)
    transcription = models.CharField(max_length=50)
    example = models.CharField(max_length=200)
    sound_url = models.CharField(max_length=50)
    theme_id = models.IntegerField()

    def __str__(self):
        return self.name


class Theme(models.Model):
    level = models.ForeignKey(Level, on_delete=models.PROTECT)
    words = models.ManyToManyField(Word)
    category_id = models.IntegerField()
    name = models.CharField(max_length=50)
    photo = models.ImageField(name='photo')
    photo_url = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Category(models.Model):
    themes = models.ForeignKey(Theme, on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    icon = models.ImageField(name='icon')
    icon_url = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
